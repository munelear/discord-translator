const {MessageAttachment} = require("discord.js");
const getUrls = require('get-urls');

const IMAGE_EXTENSIONS = ['png', 'jpg', 'jpeg', 'gif', 'gifv', 'm4v', 'mp4', 'webm'];
const MAX_ATTACHMENTS = 5;

let bot;

module.exports.init = (botRef) => {
  bot = botRef;
};

function getExtensionFromUrl(url) {
  const [uri, params] = url.split('?');
  const extension = uri.split('.').pop();

  return extension;
}

function isImageExtension(extension) {
  return IMAGE_EXTENSIONS.includes(extension.toLowerCase());
}

function isImage(url) {
  let extension = getExtensionFromUrl(url);
  if (isImageExtension(extension)) {
    return true;
  }

  return false;
}

module.exports.format = async (details) => {
  let {destination, message, translation} = details;
  const attachments = [];
  let nickname;
  let displayColor;
  let channelName;
  let imageUrl;

  if (message.guild) {
    // fetch author's nickname in the guild if the message was in a guild
    const guildMember = message.guild.member(message.author);
    nickname = guildMember ? guildMember.nickname : null;

    // set the embed color to the author's role color
    displayColor = message.member.displayColor;
    channelName = message.channel.name;
  }

  const msgAttachments = message.attachments.array();
  if (msgAttachments.length > 1) {
    bot.logger.warn(`Did not expect more than 1 file.`);
  }
  for (const attachment of msgAttachments) {
    let usedAttachment = false;
    if (!imageUrl && isImage(attachment.url)) {
      imageUrl = attachment.url;
      usedAttachment = true;
    }

    // if it's not used as the image, attach it
    if (!usedAttachment) {
      attachments.push(new MessageAttachment(
        attachment.url,
        attachment.name
      ));
    }
  }

  const urls = [...getUrls(translation)];
  // check if any of the urls in the text are images
  for (const url of urls) {
    let usedUrl = false;
    // check the URLs for images
    if (isImage(url)) {
      // if an attachment wasn't already picked as the embed image, use the first image url found
      if (!imageUrl) {
        bot.logger.debug(`Found an image in the text body, setting as the embed image: ${url}`);
        imageUrl = url;
        usedUrl = true;
      }

      if (!usedUrl) {
        // if it wasn't used as the embed image, add image urls as attachments to get the preview behavior
        bot.logger.debug(`Found additional images in the text body, adding as an attachment: ${url}`);
        attachments.push(new MessageAttachment(url));
      }
    }
  }

  const content = {
    embed: {
      author: {
        name: nickname || message.author.username,
        icon_url: message.author.displayAvatarURL()
      },
      color: displayColor,
      description: translation
    }
  };

  // if forwarding to a channel other than the original channel
  if (destination !== message.channel.id) {
    // add any attachments and set the embed's image url

    if (!bot.config.disableAttachments) {
      // drop extra attachments
      if (attachments.length > MAX_ATTACHMENTS) {
        bot.logger.warn(`[FORMAT] Dropping extra attachments {source:${message.id}, destination:${destination}, attachments:${attachments.length}}`);
        attachments = attachments.slice(0, MAX_ATTACHMENTS);
      }
      content.files = attachments;
    }

    if (imageUrl) {
      content.embed.image = { url: imageUrl };
    }

    // set the footer to the source channel's name
    if (channelName) {
      content.embed.footer = { text: `via #${channelName}` };
    }
  }

  return content;
};

module.exports.send = async (channelId, content) => {
  try {
    const forwardChannel = await bot.client.channels.fetch(channelId);

    if (forwardChannel) {
      // Check if bot can write to destination channel
      var canWriteDest = true;

      if (forwardChannel.type === "text") {
        canWriteDest = forwardChannel.permissionsFor(forwardChannel.guild.me).has("SEND_MESSAGES");
      }

      if (canWriteDest) {
        return await forwardChannel.send(content);
      } else {
        bot.logger.error(`Unable to write channel: ${channelId}`);
        if (forwardChannel.guild) {
          const owner = await bot.client.users.fetch(forwardChannel.guild.ownerID);
          await owner.send(`Help! I can't send messages to the \`#${forwardChannel.name}\` channel `+
            `in the \`'${forwardChannel.guild.name}'\` server! Please make sure I have the ` +
            `permissions to read and send messages, embed links, and attach files.`);
        }
      }
    } else {
      // couldn't find the channel?
      bot.logger.error(`Could not fetch channel: ${channelId}`);
    }
  } catch (error) {
    throw error;
  }
};