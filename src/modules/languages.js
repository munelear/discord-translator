const translate = require('@vitalets/google-translate-api');
const ISO6391 = require("iso-639-1");
const getUrls = require('get-urls');
const flagEmojiMap = require('./flagEmojiMap');
const emojiRegex = RegExp('<(a?):([^:]+):([0-9]{1,18})>', 'g');
// @userId
// @!userId - nickname
// @&roleId - role mention
// #channelId - channel mention
const mentionRegex = RegExp('<(@[&!]?|#)([0-9]{1,18})>', 'g');

// Fix inconsistencies in lang codes
// https://developers.google.com/admin-sdk/directory/v1/languages
const GOOGLE_LANG_EXCEPTIONS = {
  he: "iw",         // google uses iw for hebrew
  zh: "zh-CN",      // Chinese (PRC)
  ch: "zh-CN",      // ch is Chamorro, but assume they mean China since google doesn't translate it
  "zh-cn": "zh-CN",
  "zh-tw": "zh-TW"  // Chinese (Taiwan)
};

const REVERSE_EXCEPTION_MAP = {
  iw: "he",
  "zh-CN": "zh",
  "zh-TW": "zh"
};

let bot;

module.exports.init = (botRef) => {
  bot = botRef;
};

// map back from google supported codes to ISO codes
function getNormalizedCode(code) {
  let output = code;

  if (REVERSE_EXCEPTION_MAP.hasOwnProperty(code)) {
    output = REVERSE_EXCEPTION_MAP[code];
  }

  return output;
}

// Convert language name to google supported ISO code
function getCode(lang) {
  var code = lang.trim();

  if (!/^[A-z]{2}[A-z]?(?:-[A-z]{2,}?)?$/i.test(lang)) {
    code = ISO6391.getCode(lang);
  }

  // remap ISO codes to google supported language codes
  if (GOOGLE_LANG_EXCEPTIONS.hasOwnProperty(lang)) {
    code = GOOGLE_LANG_EXCEPTIONS[lang];
  }

  return code;
}

function getName(language) {
  const code = getCode(language);
  const normalizedCode = getNormalizedCode(code);
  return ISO6391.getName(normalizedCode);
}
module.exports.getName = getName;

function getNativeName(language) {
  const code = getCode(language);
  const normalizedCode = getNormalizedCode(code);
  return ISO6391.getNativeName(normalizedCode);
}
module.exports.getNativeName = getNativeName;

// convert language input into the google supported language codes
function getGoogleCode(lang) {
  if (!lang) return null;

  // special cases return themselves
  if (lang === "auto") return lang;

  let code = getCode(lang);
  return translate.languages.isSupported(code) ? code : null;
}
module.exports.getGoogleCode = getGoogleCode;

module.exports.getCodeFromEmoji = function (emoji) {
  let code = null;

  if (emoji && flagEmojiMap.hasOwnProperty(emoji)) {
    const language = flagEmojiMap[emoji];

    if (language.langs) {
      let i = 0;

      while (!code && i < language.langs.length) {
        code = getGoogleCode(language.langs[i++]);
      }
    }
  }

  return code;
};

function getPlaceholder() {
  return (Date.now() + ((Math.random()*100000).toFixed())).toString().slice(-12);
}

// replace URLs, mentions, and emoji with placeholders that will not be affected by translation
module.exports.preTranslate = async (content) => {
  let subs = {};

  const replace = (sourceStr, override) => {
    let replaceStr = override || sourceStr;
    let placeholder = getPlaceholder();
    content = content.replace(sourceStr, placeholder);
    subs[placeholder] = replaceStr;
  };

  const urls = [...getUrls(content, {})];
  for (const url of urls) {
    replace(url);
  }

  let regexMatch;
  const origStr = content;
  while (regexMatch = emojiRegex.exec(origStr)) {
    const [, animated, name, id] = regexMatch;
    const emojiStr = `<${animated}:${name}:${id}>`;
    const emoji = await bot.client.emojis.resolve(id);
    if (emoji) {
      replace(emojiStr);
    } else {
      // if the bot doesn't have access to the emoji, it won't appear in the final text
      // work around by linking to the emoji image CDN instead
      let extension = (animated) ? 'gif' : 'png';
      replace(emojiStr, `${emojiStr} https://cdn.discordapp.com/emojis/${id}.${extension}`);
    }
  }

  // user, nickname, role, and channel mentions
  while (regexMatch = mentionRegex.exec(origStr)) {
    const [, type, id] = regexMatch;
    const mentionStr = `<${type}${id}>`;
    replace(mentionStr);
  }

  return {content: content, subs: subs};
};

// restore placeholders with the original text after translation
function postTranslate(translation, subs) {
  let result = translation;

  if (subs) {
    for (const [key, val] of Object.entries(subs)) {
      result = result.replace(key, val);
    }
  }

  return result;
};

module.exports.translate = async function (message, to, subs, from = "auto") {
  let translated = await translate(message, { to: to, from: from });

  translated.text = postTranslate(translated.text, subs);
  return translated;
};