const moment = require('moment');

let bot;

module.exports.init = (botRef) => {
  bot = botRef;
};

function isInactive(date) {
  const cutOffDate = moment().subtract(bot.config.inactiveDays, 'days');
  return moment(date).isBefore(cutOffDate);
}

async function getTranslations(content, languages, originLanguage) {
  let detectedLanguage;
  let preTranslateObj;
  const translationMap = {};

  const getTranslation = async (content, lang) => {
    if (!translationMap[lang]) {
      if (content) {
        const translated = await bot.languages.translate(content, lang, preTranslateObj.subs);
        detectedLanguage = translated.from.language.iso;
        translationMap[lang] = translated.text;
      } else {
        // falsey/empty content
        translationMap[lang] = content;
      }
    }

    return translationMap[lang];
  };

  if (languages.length > 0 || originLanguage) {
    // strip out sensitive text that shouldn't be translated and replace with placeholders
    preTranslateObj = await bot.languages.preTranslate(content);
    const originalContent = preTranslateObj.content;

    // translate all the destination channel languages in parallel
    const translationPromises = [];
    let i = languages.length;
    while (i--) {
      translationPromises.push(getTranslation(originalContent, languages[i]));
    }
    await Promise.all(translationPromises);

    // retranslate origin if necessary
    if ((originLanguage && detectedLanguage && originLanguage != detectedLanguage) ||
        (languages.length === 0 && originLanguage)) {
      await getTranslation(originalContent, originLanguage);
    }
  }

  return {
    detectedLanguage: detectedLanguage,
    translationMap: translationMap
  };
}

module.exports.forward = async function (message) {
  try {
    const destinations = {};

    bot.logger.debug(`[EVENT] Checking if ${message.channel.id} is in a translation group`);
    // check if any translation tasks should happen and stop listening to the request
    const channel = await bot.models.channels.getById(message.channel.id);
    if (channel && channel.groupId) {
      const now = new Date();
      if (channel.paused) {
        await bot.common.resumeChannel(channel.channelId);
      } else {
        channel.lastUsedDate = now;
        await channel.save();
      }

      const channels = await bot.models.channels.getByGroupId(channel.groupId);
      if (channels.length > 0) {
        bot.logger.log(`[EVENT] Translating message {messageId: ${message.id}, author:"${message.author.tag}", authorId:"${message.author.id}", channelId:${message.channel.id}}`);
        for (const c of channels) {
          // initialize the last used date if it doesn't exist
          if (!c.lastUsedDate) {
            c.lastUsedDate = now;
            await c.save();
          }

          if (c.channelId != channel.channelId && c.language && !c.paused) {
            if (isInactive(c.lastUsedDate)) {
              await bot.common.pauseChannel(c.channelId);
            } else {
              // translate if the channel has a language configured and is not paused
              bot.logger.debug(`[EVENT] translating {messageId:${message.id}, channelId:${message.channel.id}, to:"${c.language}", destination:${c.channelId}}`);
              destinations[c.channelId] =  c.language;
            }
          }
        }

        // dedupe languages
        const languages = [...(new Set(Object.values(destinations)))];
        const translations = await getTranslations(message.content, languages, channel.language);

        // check if the message was even in the origin channel's language
        if (channel.language && translations.detectedLanguage && channel.language !== translations.detectedLanguage) {
          bot.logger.debug(`[EVENT] re-translating original message, was in ${translations.originLanguage}: {messageId:${message.id}, channelId:${message.channel.id}, to:"${channel.language}"}`);
          destinations[channel.channelId] = channel.language;
        }

        await dispatchMessages(message, destinations, translations.translationMap);
      }
    }
  } catch(error) {
    throw error;
  }
};

async function dispatchMessages(message, destinations, translationMap) {
  // forward the translated messages in parallel
  const forwardPromises = [];
  for (const [destination, language] of Object.entries(destinations)) {
    const messageIdPromise = forward(message, destination, translationMap[language]);
    messageIdPromise.then(async (translatedMsg) => {
      await bot.models.messages.addMessage(translatedMsg.id, destination, message.id, language);
    }).catch((error) => {
      bot.logger.error(`Unable to create message record: {destination:${destination}, language:"${language}", messageId:${message.id}}`, error);
    });
    forwardPromises.push(messageIdPromise);
  }
  await Promise.all(forwardPromises);
}

async function forward(message, destination, translation) {
  const payload = await bot.messages.format({
    message: message,
    destination: destination,
    translation: translation
  });
  return await bot.messages.send(destination, payload);
}

module.exports.update = async function (message) {
  try {
    const destinations = {};

    bot.logger.debug(`[EVENT] Checking if ${message.id} has any auto translated messages`);
    const messages = await bot.models.messages.getBySourceId(message.id);

    if (messages.length > 0) {
      bot.logger.log(`[EVENT] Updated message, retranslating {messageId: ${message.id}, author:"${message.author.tag}", authorId:"${message.author.id}", channelId:${message.channel.id}}`);
      let i = messages.length;
      while (i--) {
        const {channelId, messageId, language} = messages[i];
        destinations[channelId] = language;
        bot.logger.debug(`[EVENT] updating translation {messageId:${messageId}, channelId:${channelId}, language:"${language}"}`);
      }

      // dedupe languages
      const languages = [...(new Set(Object.values(destinations)))];
      const translations = await getTranslations(message.content, languages);
      await updateMessages(message, messages, translations.translationMap);
    }
  } catch(error) {
    throw error;
  }
};

async function updateMessages(message, messages, translationMap) {
  // update the translated messages in parallel
  const updatePromises = [];
  for (const msg of messages) {
    const messageIdPromise = update(message, msg.channelId, msg.messageId, translationMap[msg.language]);
    updatePromises.push(messageIdPromise);
  }
  await Promise.all(updatePromises);
}

async function update(message, channelId, messageId, translation) {
  try {
    const channel = await bot.client.channels.fetch(channelId);
    const translatedMsg = await channel.messages.fetch(messageId);

    if (translatedMsg) {
      const payload = await bot.messages.format({
        message: message,
        destination: channelId,
        translation: translation
      });
      return await translatedMsg.edit(payload);
    }
  } catch(error) {
    bot.logger.warn(`[TRANSLATE] Unable to update translated message: {messageId:${messageId}, channelId:${channelId}}`)
  }
}