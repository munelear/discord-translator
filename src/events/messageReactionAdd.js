// translate a message through discord reaction (flag)
module.exports = async (bot, reaction, user) => {
  // ignore bots and spamming the same reaction
  if (user.bot || reaction.count > 1) return;

  // Get country by emoji
  const emoji = reaction.emoji.name;
  const to = bot.languages.getCodeFromEmoji(emoji);

  // stop processing if no valid destination language code
  if (!to) return;

  try {
    // translate the message that was reacted to
    const preTranslateObj = await bot.languages.preTranslate(reaction.message.content);
    const translated = await bot.languages.translate(preTranslateObj.content, to, preTranslateObj.subs);

    const content = await bot.messages.format({
      message: reaction.message,
      translation: translated.text,
      destination: reaction.message.channel.id
    });

    const translatedMsg = await bot.messages.send(reaction.message.channel.id, content);
    await bot.models.messages.addMessage(translatedMsg.id, reaction.message.channel.id, reaction.message.id, to);
  } catch(error) {
    return bot.logger.error(error);
  }
};