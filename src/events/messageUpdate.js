module.exports = async (bot, oldMessage, newMessage) => {
  if (oldMessage && newMessage && oldMessage.content != newMessage.content) {
    // check if any messages need to also be updated
    await bot.auto.update(newMessage);
  }
};