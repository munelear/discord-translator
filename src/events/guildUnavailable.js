module.exports = (bot, guild) => {
  bot.logger.warn("Guild unavailable: " + guild.id);
};