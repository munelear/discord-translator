module.exports = async (bot, message) => {
  const messages = await bot.models.messages.getBySourceId(message.id);

  if (messages.length > 0) {
    bot.logger.log(`[DELETE] Detected deleted message: {messageId:${message.id}}`);
    const deletePromises = [];
    for (const msg of messages) {
      try {
        bot.logger.debug(`[DELETE] Deleting: {source:${message.id}, messageId:${msg.messageId}, channelId:${msg.channelId}}`);
        const channel = await bot.client.channels.fetch(msg.channelId);
        const translatedMsg = await channel.messages.fetch(msg.messageId);
        deletePromises.push(translatedMsg.delete());
      } catch(error) {
        bot.logger.warn(`[DELETE] Unable to fetch message to delete: {source:${message.id}, destination:${msg.messageId}`);
      }

      // remove record of the message
      await bot.models.messages.findByIdAndDelete(msg._id);
    }

    try {
      await Promise.all(deletePromises);
    } catch(error) {
      bot.logger.warn(`[DELETE] Encountered errors deleteing messages: ${error}`);
    }
  }
};