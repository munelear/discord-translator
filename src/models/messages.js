const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
  messageId: {type: String, required: true},
  channelId: {type: String, required: true},
  sourceMessageId: {type:String, required: true},
  language: {type:String, required: true},
  time: {type: Date, default: new Date()}
});

messageSchema.statics.getBySourceId = async function(messageId) {
  return await this.find({sourceMessageId: messageId}).exec();
};

messageSchema.statics.addMessage = async function(messageId, channelId, sourceMessageId, language) {
  return await this.findOneAndUpdate({
    messageId: messageId,
    channelId: channelId,
    sourceMessageId: sourceMessageId,
    language: language
  }, {
    messageId: messageId,
    channelId: channelId,
    sourceMessageId: sourceMessageId,
    language: language
  }, {
    upsert: true,
    new: true,
    setDefaultsOnInsert: true
  }).exec();
};

const Message = mongoose.model('messages', messageSchema);
module.exports = Message;